package edu.luc.etl.cs313.android.shapes.model;
import java.util.Iterator;
import java.util.List;


/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {
		final Iterator<? extends Shape> it = g.getShapes().iterator();
		int x_max = 0;
		int y_max = 0;
		int x_min = Integer.MAX_VALUE;
		int y_min = Integer.MAX_VALUE;

		while (it.hasNext()) {
			final Location l = it.next().accept(this);
			int x = l.getX();
			int y = l.getY();
			int width_x = x;
			int height_y = y;
			if (l.getShape() instanceof Rectangle)
			{
				width_x += ((Rectangle) l.getShape()).getWidth();
			}
			if(l.getShape() instanceof Rectangle)
			{
				height_y += ((Rectangle) l.getShape()).getHeight();
			}
			if (x_max  <= width_x) {
				x_max  = width_x;
			}
			if (y_max <= height_y)
			{
				y_max = height_y;
			}
			if (x_min >= x)
			{
				x_min = x;
			}
			if (y_min >= y) {
				y_min = y;
			}
		}
		return new Location(x_min, y_min, new Rectangle((x_max - x_min),
				(y_max - y_min)));
	}

	@Override
	public Location onLocation(final Location l) {
		final Location mylo = l.getShape().accept(this);
		final int x = l.getX() + mylo.getX();
		final int y = l.getY() + mylo.getY();
		return new Location(x,y,mylo.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		final int width = r.getWidth();
		final int height = r.getHeight();
		return new Location(0,0, new Rectangle(width, height));
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {
		Location l;
		final Shape s = c.getShape();
		l = s.accept(this);
		return l;
	}

	@Override
	public Location onOutline(final Outline o) {
		Location l;
		final Shape s = o.getShape();
		l = s.accept(this);
		return l;
	}

	@Override
	public Location onPolygon(final Polygon s) {
		List<? extends Point> points = s.getPoints();
		Point point;
		int x_large = 0, y_large=0, x_small=0, y_small=0;
		int x,y,width,height,
				x_1,y_1;

		x_1 = points.get(0).getY();
		y_1 = points.get(0).getY();
		x_small =
				x_1;
		y_small
				= y_1;
		x_large=
				x_1;
		y_large=y_1;
		for(int i=0; i<points.size(); i++){
			point = points.get(i);
			x=point.getX();
			y=point.getY();
			if(x>x_large)
			{
				x_large = x;
			}
			if(y>y_large)
			{
				y_large = y;
			}
		}
		width = x_large - x_small;
		height = y_large - y_small
		;
		return new Location(
				x_1, y_1, new Rectangle(width, height));
	}
}
